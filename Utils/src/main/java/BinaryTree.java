import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Queue;

public class BinaryTree<T> implements Tree<T> {

    private BinaryTreeNode<T>[] root;
    private int size;
    private static final int DEFAULT_CAPACITY = 10;
    private static final int MAX_ARRAY_SIZE = Integer.MAX_VALUE - 8;

    public BinaryTree() {
        root = new BinaryTreeNode[DEFAULT_CAPACITY];
    }

    @Override
    public int height() {
        if (root == null) {
            return 0;
        }
        return height(root[0]);
    }

    private int height(BinaryTreeNode<T> root) {
        if (root == null) {
            return 0;
        }
        return Math.max(height(root.getLeft()), height(root.getRight())) + 1;
    }

    @Override
    public boolean isNullOrEmpty() {
        return root == null || size == 0;
    }

    @Override
    public boolean contains(T o) {
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }

    private void ensureCapacityInternal(int minCapacity) {
        ensureExplicitCapacity(Math.max(DEFAULT_CAPACITY, minCapacity));
    }

    private void ensureExplicitCapacity(int minCapacity) {
        if (minCapacity - root.length > 0)
            grow(minCapacity);
    }

    private void grow(int minCapacity) {
        int oldCapacity = root.length;
        int newCapacity = oldCapacity + (oldCapacity >> 1);
        if (newCapacity - minCapacity < 0)
            newCapacity = minCapacity;
        if (newCapacity - MAX_ARRAY_SIZE > 0)
            newCapacity = hugeCapacity(minCapacity);
        root = Arrays.copyOf(root, newCapacity);
    }

    private static int hugeCapacity(int minCapacity) {
        if (minCapacity < 0) throw new OutOfMemoryError();
        return (minCapacity > MAX_ARRAY_SIZE) ? Integer.MAX_VALUE : MAX_ARRAY_SIZE;
    }

    @Override
    public void insert(T data) {
        ensureCapacityInternal(size + 1);  // Increments modCount!!
        if (size == 0) {
            root[size++] = new BinaryTreeNode<>(data);
        } else {
            root[size++] = new BinaryTreeNode<>(data);
            if (root[(size / 2) - 1].getLeft() == null) {
                root[(size / 2) - 1].setLeft(root[size - 1]);
            } else if (root[(size / 2) - 1].getRight() == null) {
                root[(size / 2) - 1].setRight(root[size - 1]);
            }
        }
    }

    @Override
    public void insertAll(T[] node) {
        Arrays.stream(node).forEach(this::insert);
    }

    @Override
    public boolean delete() {
        return false;
    }

    @Override
    public void traverse() {
        preOrderTraversal(root[0]);
        System.out.println();
    }

    public void preOrderTraversal() {
        preOrderTraversal(root[0]);
        System.out.println();
    }

    private void preOrderTraversal(BinaryTreeNode<T> root) {
        if (root == null) {
            return;
        }
        System.out.print(root.getData() + " ");
        preOrderTraversal(root.getLeft());
        preOrderTraversal(root.getRight());
    }

    public void postOrderTraversal() {
        postOrderTraversal(root[0]);
        System.out.println();
    }

    private void postOrderTraversal(BinaryTreeNode<T> root) {
        if (root == null) {
            return;
        }
        postOrderTraversal(root.getLeft());
        postOrderTraversal(root.getRight());
        System.out.print(root.getData() + " ");
    }

    public void inorderOrderTraversal() {
        inorderOrderTraversal(root[0]);
        System.out.println();
    }

    private void inorderOrderTraversal(BinaryTreeNode<T> root) {
        if (root == null) {
            return;
        }
        inorderOrderTraversal(root.getLeft());
        System.out.print(root.getData() + " ");
        inorderOrderTraversal(root.getRight());
    }

    public void levelOrderTraversal() {
        levelOrderTraversal(root[0], false);
        System.out.println();
    }

    public void levelOrderTraversalLineByLine() {
        levelOrderTraversal(root[0], true);
    }

    private void levelOrderTraversal(BinaryTreeNode<T> root, boolean lineByLine) {
        if (root == null) {
            return;
        }
        Queue<BinaryTreeNode<T>> queue = new ArrayDeque<>();
        queue.add(root);
        int count = queue.size();
        while (!queue.isEmpty()) {
            BinaryTreeNode<T> node = queue.poll();
            System.out.print(node.getData() + " ");
            if (node.getLeft() != null)
                queue.add(node.getLeft());
            if (node.getRight() != null)
                queue.add(node.getRight());

            if (lineByLine) {
                count--;
                if (count == 0) {
                    System.out.println();
                    count = queue.size();
                }
            }
        }
    }

    //using height
    public void levelOrderTraversalUsingHeight() {
        int h = height();
        for (int i = 0; i < h; i++) {
            levelOrderTraversalUsingHeight(root[0], i);
        }
        System.out.println();
    }

    public void levelOrderTraversalUsingHeightLineByLine() {
        int h = height();
        for (int i = 0; i < h; i++) {
            levelOrderTraversalUsingHeight(root[0], i);
            //for line by line print level order traversal
            System.out.println();
        }
    }

    private void levelOrderTraversalUsingHeight(BinaryTreeNode<T> root, int level) {
        if (root == null) {
            return;
        }
        if (level == 0) {
            System.out.print(root.getData() + " ");
        } else {
            levelOrderTraversalUsingHeight(root.getLeft(), level - 1);
            levelOrderTraversalUsingHeight(root.getRight(), level - 1);
        }
    }

    public void leastCommonAncestor(T data1, T data2){

    }
}
