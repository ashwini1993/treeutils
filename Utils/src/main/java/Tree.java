import java.util.Iterator;

public interface Tree<T> {

    int height();

    boolean isNullOrEmpty();

    boolean contains(T o);

    Iterator<T> iterator();

    void insert(T data);

    void insertAll(T[] node);

    boolean delete();

    void traverse();

}
