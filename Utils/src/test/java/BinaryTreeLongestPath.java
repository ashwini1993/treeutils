public class BinaryTreeLongestPath {

    public static void main(String[] args) {
        BinaryTree<Integer> tree = new BinaryTree<>();
        tree.insertAll(new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12});
        System.out.println(tree.height());
        tree.traverse();
        tree.inorderOrderTraversal();
        tree.postOrderTraversal();
        tree.preOrderTraversal();
        tree.levelOrderTraversal();
        tree.levelOrderTraversalLineByLine();
        tree.levelOrderTraversalUsingHeight();
        tree.levelOrderTraversalUsingHeightLineByLine();
    }
}
